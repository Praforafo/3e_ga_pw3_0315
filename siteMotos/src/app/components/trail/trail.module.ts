import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrailComponent } from './trail.component';



@NgModule({
  declarations: [
    TrailComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TrailModule { }
