import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SportComponent } from './sport.component';



@NgModule({
  declarations: [
    SportComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SportModule { }
