import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TrailComponent } from './components/trail/trail.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CadastroClienteComponent } from './components/cadastro-cliente/cadastro-cliente.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { CustomComponent } from './components/custom/custom.component';
import { LoginComponent } from './components/login/login.component';
import { SportComponent } from './components/sport/sport.component';
import { StreetComponent } from './components/street/street.component';
import { TouringComponent } from './components/touring/touring.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'cadastro-clientes', component: CadastroClienteComponent},
  {path: 'cadastro-motos', component: CadastroMotosComponent},
  {path: 'pedidos', component: PedidosComponent},
  {path: 'cadastro-usuario', component: CadastroUsuarioComponent},
  {path: 'custom', component: CustomComponent},
  {path: 'login', component: LoginComponent},
  {path: 'scooter', component: ScooterComponent},
  {path: 'sport', component: SportComponent},
  {path: 'street', component: StreetComponent},
  {path: 'touring', component: TouringComponent},
  {path: 'trail', component: TrailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
